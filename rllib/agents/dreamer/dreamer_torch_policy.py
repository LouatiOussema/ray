import logging

import ray
from ray.rllib.policy.torch_policy_template import build_torch_policy
from ray.rllib.agents.a3c.a3c_torch_policy import apply_grad_clipping
from ray.rllib.utils.framework import try_import_torch
from ray.rllib.models.catalog import ModelCatalog
from ray.rllib.agents.dreamer.utils import FreezeParameters
from ray.rllib.agents.dreamer.dreamer_model import env_select

MAZE = env_select()

torch, nn = try_import_torch()
if torch:
    from torch import distributions as td

logger = logging.getLogger(__name__)


# This is the computation graph for workers (inner adaptation steps)
def compute_dreamer_loss(obs,
                         mask,
                         action,
                         reward,
                         model,
                         imagine_horizon,
                         discount=0.99,
                         lambda_=0.95,
                         kl_coeff=1.0,
                         free_nats=3.0,
                         log=False):
    """Constructs loss for the Dreamer objective

        Args:
            obs (TensorType): Observations (o_t)
            action (TensorType): Actions (a_(t-1))
            reward (TensorType): Rewards (r_(t-1))
            model (TorchModelV2): DreamerModel, encompassing all other models
            imagine_horizon (int): Imagine horizon for actor and critic loss
            discount (float): Discount
            lambda_ (float): Lambda, like in GAE
            kl_coeff (float): KL Coefficient for Divergence loss in model loss
            free_nats (float): Threshold for minimum divergence in model loss
            log (bool): If log, generate gifs
        """
    assert reward.ndim == 2, "compute_dreamer_loss reward shape {0}".format(reward.shape)
    B, T = reward.shape
    if MAZE:
        assert action.ndim == 2 and action.shape[0] == B and action.shape[1] == T, "compute dreamer_loss action shape {0}".format(action.shape)
        action = torch.zeros(B, T, 49).scatter(-1, action.long().unsqueeze(-1), -1)
        assert mask.ndim == 3 and mask.shape[0] == B and mask.shape[1] == T and mask.shape[2] == 49, "compute_dreamer_loss mask shape {0}".format(mask.shape)
    else:
        assert action.ndim == 3 and action.shape[0] == B and action.shape[1] == T and action.shape[2] == 3, "compute_dreamer_loss action shape {0}".format(action.shape)
    assert obs.ndim == 5 and obs.shape[0] == B and obs.shape[1] == T and obs.shape[2] == 3 and obs.shape[3] == 64 and obs.shape[4] == 64, "compute_dreamer_loss obs shape {0}".format(obs.shape)

    encoder_weights = list(model.encoder.parameters())
    decoder_weights = list(model.decoder.parameters())
    if MAZE:
        mask_decoder_weights = list(model.mask_decoder.parameters())
    else:
        mask_decoder_weights = []
    reward_weights = list(model.reward.parameters())
    dynamics_weights = list(model.dynamics.parameters())
    critic_weights = list(model.value.parameters())
    model_weights = list(encoder_weights + decoder_weights + mask_decoder_weights + reward_weights +
                         dynamics_weights)

    device = (torch.device("cuda")
              if torch.cuda.is_available() else torch.device("cpu"))
    # PlaNET Model Loss
    latent = model.encoder(obs.float())
    post, prior = model.dynamics.observe(latent, action)
    features = model.dynamics.get_feature(post)
    image_pred = model.decoder(features)
    if MAZE:
        mask_pred = model.mask_decoder(features)
    reward_pred = model.reward(features)
    image_loss = -torch.mean(image_pred.log_prob(obs))
    if MAZE:
        mask_loss = -torch.mean(mask_pred.log_prob(mask))
    reward_loss = -torch.mean(reward_pred.log_prob(reward))
    prior_dist = model.dynamics.get_dist(prior[0], prior[1])
    post_dist = model.dynamics.get_dist(post[0], post[1])
    div = torch.mean(
        torch.distributions.kl_divergence(post_dist, prior_dist).sum(dim=2))
    div = torch.clamp(div, min=free_nats)
    model_loss = kl_coeff * div + reward_loss + image_loss
    if MAZE:
        model_loss += mask_loss

    # Actor Loss
    # [imagine_horizon, batch_length*batch_size, feature_size]
    with torch.no_grad():
        actor_states = [v.detach() for v in post]
    with FreezeParameters(model_weights):
        imag_feat = model.imagine_ahead(actor_states, imagine_horizon)
    with FreezeParameters(model_weights + critic_weights):
        reward = model.reward(imag_feat).mean
        value = model.value(imag_feat).mean
    pcont = discount * torch.ones_like(reward)
    returns = lambda_return(reward[:-1], value[:-1], pcont[:-1], value[-1], lambda_)
    discount_shape = pcont[:1].size()
    discount = torch.cumprod(
        torch.cat([torch.ones(*discount_shape).to(device), pcont[:-2]], dim=0),
        dim=0)
    actor_loss = -torch.mean(discount * returns)

    # Critic Loss
    with torch.no_grad():
        val_feat = imag_feat.detach()[:-1]
        target = returns.detach()
        val_discount = discount.detach()
    val_pred = model.value(val_feat)
    critic_loss = -torch.mean(val_discount * val_pred.log_prob(target))

    # Logging purposes
    prior_ent = torch.mean(prior_dist.entropy())
    post_ent = torch.mean(post_dist.entropy())

    return_dict = {
        "model_loss": model_loss,
        "reward_loss": reward_loss,
        "image_loss": image_loss,
        "divergence": div,
        "actor_loss": actor_loss,
        "critic_loss": critic_loss,
        "prior_ent": prior_ent,
        "post_ent": post_ent,
    }
    if MAZE:
        return_dict['mask_loss'] = mask_loss
    return return_dict


# Similar to GAE-Lambda, calculate value targets
def lambda_return(reward, value, pcont, bootstrap, lambda_):
    def agg_fn(x, y):
        return y[0] + y[1] * lambda_ * x

    next_values = torch.cat([value[1:], bootstrap[None]], dim=0)
    inputs = reward + pcont * next_values * (1 - lambda_)

    last = bootstrap
    returns = []
    for i in reversed(range(len(inputs))):
        last = agg_fn(last, [inputs[i], pcont[i]])
        returns.append(last)

    returns = list(reversed(returns))
    returns = torch.stack(returns, dim=0)
    return returns


def dreamer_loss(policy, model, dist_class, train_batch):
    log_gif = False

    if MAZE:
        policy.stats_dict = compute_dreamer_loss(
            train_batch["obs"],
            train_batch["mask"],
            train_batch["actions"],
            train_batch["rewards"],
            policy.model,
            policy.config["imagine_horizon"],
            policy.config["discount"],
            policy.config["lambda"],
            policy.config["kl_coeff"],
            policy.config["free_nats"],
            log_gif,
        )
    else:
        policy.stats_dict = compute_dreamer_loss(
            train_batch["obs"],
            None,
            train_batch["actions"],
            train_batch["rewards"],
            policy.model,
            policy.config["imagine_horizon"],
            policy.config["discount"],
            policy.config["lambda"],
            policy.config["kl_coeff"],
            policy.config["free_nats"],
            log_gif,
        )

    loss_dict = policy.stats_dict

    return (loss_dict["model_loss"], loss_dict["actor_loss"],
            loss_dict["critic_loss"])


def build_dreamer_model(policy, obs_space, action_space, config):

    policy.model = ModelCatalog.get_model_v2(
        obs_space,
        action_space,
        1,
        config["dreamer_model"],
        name="DreamerModel",
        framework="torch")

    policy.model_variables = policy.model.variables()

    return policy.model


def action_sampler_fn(policy, model, input_dict, state, explore, timestep):
    """Action sampler function has two phases. During the prefill phase,
    actions are sampled uniformly [-1, 1]. During training phase, actions
    are evaluated through DreamerPolicy and an additive gaussian is added
    to incentivize exploration.
    """

    obs = input_dict["obs"]

    if MAZE:
        mask = obs[:, -49:].reshape(-1, 49)
        obs = obs[:, 3:-49].reshape(-1, 3, 64, 64)
        random_actions = torch.multinomial(mask.float() + 1e-6, 1).squeeze(-1)

    assert obs.ndim == 4 and obs.shape[0] == 1 and obs.shape[1] == 3 and obs.shape[2] == 64 and obs.shape[3] == 64, "action_sampler_fn obs shape {0}".format(obs.shape)

    # Custom Exploration
    if timestep <= policy.config["prefill_timesteps"]:
        logp = torch.tensor([0.0])
        # Random action in space [-1.0, 1.0]
        if MAZE:
            action = random_actions
        else:
            action = 2.0 * torch.rand(1, model.action_space.shape[0]) - 1.0
        state = model.get_initial_state()
    else:
        # Weird RLLib Handling, this happens when env rests
        if len(state[0].size()) == 3 or len(state[1].size()) == 3 or len(state[2].size()) == 3 or len(state[3].size()) == 3 :
            # Very hacky, but works on all envs
            state = model.get_initial_state()
        if MAZE:
            action, logp, state = model.policy(obs.float(), mask.float(), state, explore)
        else:
            action, logp, state = model.policy(obs.float(), None, state, explore)
        if MAZE:
            exploration_indices = torch.rand(action.shape[0]) < policy.config["explore_noise"]
            action = torch.where(exploration_indices, random_actions, action)
        else:
            action = td.Normal(action, policy.config["explore_noise"]).sample()
            action = torch.clamp(action, min=-1.0, max=1.0)

    policy.global_timestep += policy.config["action_repeat"]

    assert logp.ndim == 1 and logp.shape[0] == 1, "action_sampler_fn logp shape {0}".format(logp.shape)
    if MAZE:
        assert action.ndim == 1 and action.shape[0] == 1, "action_sampler_fn action shape {0}".format(action.shape)

    else:
        assert action.ndim == 2 and action.shape[0] == 1 and action.shape[1] == 3, "action_sampler_fn action shape {0}".format(action.shape)

    return action, logp, state


def dreamer_stats(policy, train_batch):
    return policy.stats_dict


def dreamer_optimizer_fn(policy, config):
    model = policy.model
    encoder_weights = list(model.encoder.parameters())
    decoder_weights = list(model.decoder.parameters())
    reward_weights = list(model.reward.parameters())
    dynamics_weights = list(model.dynamics.parameters())
    actor_weights = list(model.actor.parameters())
    critic_weights = list(model.value.parameters())
    model_opt = torch.optim.Adam(
        encoder_weights + decoder_weights + reward_weights + dynamics_weights,
        lr=config["td_model_lr"])
    actor_opt = torch.optim.Adam(actor_weights, lr=config["actor_lr"])
    critic_opt = torch.optim.Adam(critic_weights, lr=config["critic_lr"])

    return (model_opt, actor_opt, critic_opt)


DreamerTorchPolicy = build_torch_policy(
    name="DreamerTorchPolicy",
    get_default_config=lambda: ray.rllib.agents.dreamer.dreamer.DEFAULT_CONFIG,
    action_sampler_fn=action_sampler_fn,
    loss_fn=dreamer_loss,
    stats_fn=dreamer_stats,
    make_model=build_dreamer_model,
    optimizer_fn=dreamer_optimizer_fn,
    extra_grad_process_fn=apply_grad_clipping)
