import numpy as np
from typing import Any, List, Tuple
from ray.rllib.models.torch.misc import Reshape
from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
from ray.rllib.utils.framework import try_import_torch
from ray.rllib.utils.framework import TensorType

MAZE = True


def env_select():
    return MAZE


torch, nn = try_import_torch()
if torch:
    from torch import distributions as td
    from ray.rllib.agents.dreamer.utils import Linear, Conv2d, \
        ConvTranspose2d, GRUCell, TanhBijector

ActFunc = Any


# Encoder, part of PlaNET
class ConvEncoder(nn.Module):
    """Standard Convolutional Encoder for Dreamer. This encoder is used
    to encode images frm an enviornment into a latent state for the
    RSSM model in PlaNET.
    """

    def __init__(self,
                 depth: int = 32,
                 act: ActFunc = None,
                 shape: Tuple[int] = (3, 64, 64)):
        """Initializes Conv Encoder

        Args:
            depth (int): Number of channels in the first conv layer
            act (Any): Activation for Encoder, default ReLU
            shape (List): Shape of observation input
        """
        super().__init__()
        self.act = act
        if not act:
            self.act = nn.ReLU
        self.depth = depth
        self.shape = shape

        init_channels = self.shape[0]
        self.layers = [
            Conv2d(init_channels, self.depth, 4, stride=2),
            self.act(),
            Conv2d(self.depth, 2 * self.depth, 4, stride=2),
            self.act(),
            Conv2d(2 * self.depth, 4 * self.depth, 4, stride=2),
            self.act(),
            Conv2d(4 * self.depth, 8 * self.depth, 4, stride=2),
            self.act(),
        ]
        self.model = nn.Sequential(*self.layers)

    def forward(self, x):
        assert x.ndim == 4 or x.ndim == 5, "ConvEncoder input shape {0}".format(x.shape)
        assert x.shape[-1] == 64 and x.shape[-2] == 64 and x.shape[-3] == 3, "ConvEncoder input shape {0}".format(
            x.shape)
        B = x.shape[0]
        if x.ndim == 5:
            T = x.shape[1]
        # Flatten to [batch*horizon, 3, 64, 64] in loss function
        orig_shape = list(x.size())
        x = x.view(-1, *(orig_shape[-3:]))
        x = self.model(x)

        new_shape = orig_shape[:-3] + [32 * self.depth]
        x = x.view(*new_shape)
        assert x.ndim == 2 or x.ndim == 3, "ConvEncoder output shape {0}".format(x.shape)
        assert x.shape[-1] == 384 and x.shape[0] == B, "ConvEncoder output shape {0}".format(x.shape)
        if x.ndim == 3:
            assert x.shape[1] == T, "ConvEncoder output shape {0}".format(x.shape)
        return x


# Decoder, part of PlaNET
class ConvDecoder(nn.Module):
    """Standard Convolutional Decoder for Dreamer.

    This decoder is used to decode images from the latent state generated
    by the transition dynamics model. This is used in calculating loss and
    logging gifs for imagined trajectories.
    """

    def __init__(self,
                 input_size: int,
                 depth: int = 32,
                 act: ActFunc = None,
                 shape: Tuple[int] = (3, 64, 64)):
        """Initializes a ConvDecoder instance.

        Args:
            input_size (int): Input size, usually feature size output from
                RSSM.
            depth (int): Number of channels in the first conv layer
            act (Any): Activation for Encoder, default ReLU
            shape (List): Shape of observation input
        """
        super().__init__()
        self.act = act
        if not act:
            self.act = nn.ReLU
        self.depth = depth
        self.shape = shape

        self.layers = [
            Linear(input_size, 32 * self.depth),
            Reshape([-1, 32 * self.depth, 1, 1]),
            ConvTranspose2d(32 * self.depth, 4 * self.depth, 5, stride=2),
            self.act(),
            ConvTranspose2d(4 * self.depth, 2 * self.depth, 5, stride=2),
            self.act(),
            ConvTranspose2d(2 * self.depth, self.depth, 6, stride=2),
            self.act(),
            ConvTranspose2d(self.depth, self.shape[0], 6, stride=2),
        ]
        self.model = nn.Sequential(*self.layers)

    def forward(self, x):
        assert x.ndim == 3, "ConvDecoder input shape {0}".format(x.shape)
        B, T, _ = x.shape
        # x is [batch, hor_length, input_size]
        orig_shape = list(x.size())
        x = self.model(x)
        reshape_size = orig_shape[:-1] + list(self.shape)
        mean = x.view(*reshape_size)

        assert mean.ndim == 5 and mean.shape[0] == B and mean.shape[1] == T and mean.shape[2] == 3 and mean.shape[
            3] == 64 and mean.shape[4] == 64, "ConvDecoder output shape {0}".format(mean.shape)

        # Equivalent to making a multivariate diag
        return td.Independent(td.Normal(mean, 1), len(self.shape))


# Reward Model (PlaNET), and Value Function
class DenseDecoder(nn.Module):
    """FC network that outputs a distribution for calculating log_prob.

    Used later in DreamerLoss.
    """

    def __init__(self,
                 input_size: int,
                 output_size: int,
                 layers: int,
                 units: int,
                 dist: str = "normal",
                 act: ActFunc = None):
        """Initializes FC network

        Args:
            input_size (int): Input size to network
            output_size (int): Output size to network
            layers (int): Number of layers in network
            units (int): Size of the hidden layers
            dist (str): Output distribution, parameterized by FC output
                logits.
            act (Any): Activation function
        """
        super().__init__()
        self.layrs = layers
        self.units = units
        self.act = act
        if not act:
            self.act = nn.ELU
        self.dist = dist
        self.input_size = input_size
        self.output_size = output_size
        self.layers = []
        cur_size = input_size
        for _ in range(self.layrs):
            self.layers.extend([Linear(cur_size, self.units), self.act()])
            cur_size = units
        self.layers.append(Linear(cur_size, output_size))
        self.model = nn.Sequential(*self.layers)

    def forward(self, x):
        assert x.ndim == 2 or x.ndim == 3, "DenseDecoder input shape {0}".format(x.shape)
        B = x.shape[0]
        has_time = False
        if x.ndim == 3:
            T = x.shape[1]
            has_time = True
        x = self.model(x)
        assert (x.ndim == 2 or x.ndim == 3) and x.shape[0] == B, "DenseDecoder output shape {0}".format(x.shape)
        if has_time:
            assert x.shape[1] == T, "DenseDecoder output shape {0}".format(x.shape)
        if self.output_size == 1:
            x = torch.squeeze(x)
        if self.dist == "normal":
            output_dist = td.Normal(x, 1)
        elif self.dist == "binary":
            output_dist = td.Bernoulli(logits=x)
        else:
            raise NotImplementedError("Distribution type not implemented!")
        return td.Independent(output_dist, 0)


# Represents dreamer policy
class ActionDecoder(nn.Module):
    """ActionDecoder is the policy module in Dreamer.

    It outputs a distribution parameterized by mean and std, later to be
    transformed by a custom TanhBijector in utils.py for Dreamer.
    """

    def __init__(self,
                 input_size: int,
                 action_size: int,
                 layers: int,
                 units: int,
                 act: ActFunc = None,
                 min_std: float = 1e-4,
                 init_std: float = 5.0,
                 mean_scale: float = 5.0):
        """Initializes Policy

        Args:
            input_size (int): Input size to network
            action_size (int): Action space size
            layers (int): Number of layers in network
            units (int): Size of the hidden layers
            act (Any): Activation function
            min_std (float): Minimum std for output distribution
            init_std (float): Intitial std
            mean_scale (float): Augmenting mean output from FC network
        """
        super().__init__()
        self.layrs = layers
        self.units = units
        self.act = act
        if not act:
            self.act = nn.ReLU
        self.min_std = min_std
        self.init_std = init_std
        self.mean_scale = mean_scale
        self.action_size = action_size

        self.layers = []
        self.softplus = nn.Softplus()

        # MLP Construction
        cur_size = input_size
        for _ in range(self.layrs):
            self.layers.extend([Linear(cur_size, self.units), self.act()])
            cur_size = self.units
        if MAZE:
            self.layers.append(Linear(cur_size, action_size))
        else:
            self.layers.append(Linear(cur_size, 2 * action_size))
        self.model = nn.Sequential(*self.layers)

    # Returns distribution
    def forward(self, x, mask=None):
        assert x.ndim == 2, "ActionDecoder input shape {0}".format(x.shape)
        B = x.shape[0]
        raw_init_std = np.log(np.exp(self.init_std) - 1)
        x = self.model(x)
        if MAZE:
            if mask is not None:
                x = x - 1e9 * (1 - mask)
            assert x.ndim == 2 and x.shape[0] == B and x.shape[
                1] == self.action_size, "ActionDecoder output shape {0}".format(x.shape)
            dist = td.RelaxedOneHotCategorical(temperature=torch.tensor([1.]), logits=x)
        else:
            mean, std = torch.chunk(x, 2, dim=-1)
            mean = self.mean_scale * torch.tanh(mean / self.mean_scale)
            std = self.softplus(std + raw_init_std) + self.min_std
            assert mean.ndim == 2 and mean.shape[0] == B and mean.shape[
                1] == self.action_size, "ActionDecoder output shape {0}".format(mean.shape)
            assert std.ndim == 2 and std.shape[0] == B and std.shape[
                1] == self.action_size, "ActionDecoder output shape {0}".format(std.shape)
            dist = td.Normal(mean, std)
            transforms = [TanhBijector()]
            dist = td.transformed_distribution.TransformedDistribution(
                dist, transforms)
            dist = td.Independent(dist, 1)
        return dist


# Represents TD model in PlaNET
class RSSM(nn.Module):
    """RSSM is the core recurrent part of the PlaNET module. It consists of
    two networks, one (obs) to calculate posterior beliefs and states and
    the second (img) to calculate prior beliefs and states. The prior network
    takes in the previous state and action, while the posterior network takes
    in the previous state, action, and a latent embedding of the most recent
    observation.
    """

    def __init__(self,
                 action_size: int,
                 embed_size: int,
                 stoch: int = 30,
                 deter: int = 200,
                 hidden: int = 200,
                 act: ActFunc = None):
        """Initializes RSSM

        Args:
            action_size (int): Action space size
            embed_size (int): Size of ConvEncoder embedding
            stoch (int): Size of the distributional hidden state
            deter (int): Size of the deterministic hidden state
            hidden (int): General size of hidden layers
            act (Any): Activation function
        """
        super().__init__()
        self.stoch_size = stoch
        self.deter_size = deter
        self.hidden_size = hidden
        self.action_size = action_size
        self.act = act
        if act is None:
            self.act = nn.ELU

        self.obs1 = Linear(embed_size + deter, hidden)
        self.obs2 = Linear(hidden, 2 * stoch)

        self.cell = GRUCell(self.hidden_size, hidden_size=self.deter_size)
        self.img1 = Linear(stoch + action_size, hidden)
        self.img2 = Linear(deter, hidden)
        self.img3 = Linear(hidden, 2 * stoch)

        self.softplus = nn.Softplus

        self.device = (torch.device("cuda")
                       if torch.cuda.is_available() else torch.device("cpu"))

    def get_initial_state(self, batch_size: int) -> List[TensorType]:
        """Returns the inital state for the RSSM, which consists of mean,
        std for the stochastic state, the sampled stochastic hidden state
        (from mean, std), and the deterministic hidden state, which is
        pushed through the GRUCell.

        Args:
            batch_size (int): Batch size for initial state

        Returns:
            List of tensors
        """
        return [
            torch.zeros(batch_size, self.stoch_size).to(self.device),
            torch.zeros(batch_size, self.stoch_size).to(self.device),
            torch.zeros(batch_size, self.stoch_size).to(self.device),
            torch.zeros(batch_size, self.deter_size).to(self.device),
        ]

    def observe(self,
                embed: TensorType,
                action: TensorType,
                state: List[TensorType] = None
                ) -> Tuple[List[TensorType], List[TensorType]]:
        """Returns the corresponding states from the embedding from ConvEncoder
        and actions. This is accomplished by rolling out the RNN from the
        starting state through eacn index of embed and action, saving all
        intermediate states between.

        Args:
            embed (TensorType): ConvEncoder embedding
            action (TensorType): Actions
            state (List[TensorType]): Initial state before rollout

        Returns:
            Posterior states and prior states (both List[TensorType])
        """
        if state is None:
            state = self.get_initial_state(action.size()[0])

        assert embed.ndim == 3, "RSSM observe input embed shape {0}".format(embed.shape)
        B, T, _ = embed.shape
        assert action.ndim == 3 and action.shape[0] == B and action.shape[1] == T and action.shape[
            2] == self.action_size, "RSSM observe input action shape {0}".format(action.shape)
        assert len(state) == 4, "RSSM observe input state length {0}".format(len(state))
        for i in range(4):
            assert state[i].ndim == 2 and state[i].shape[0] == B, "RSSM observe input state {0} shape {1}".format(i,
                                                                                                                  state[
                                                                                                                      i].shape)
            if i == 3:
                assert state[i].shape[1] == self.deter_size, "RSSM observe input state {0} shape {1}".format(i, state[
                    i].shape)
            else:
                assert state[i].shape[1] == self.stoch_size, "RSSM observe input state {0} shape {1}".format(i, state[
                    i].shape)

        embed = embed.permute(1, 0, 2)
        action = action.permute(1, 0, 2)

        priors = [[] for i in range(len(state))]
        posts = [[] for i in range(len(state))]
        last = (state, state)
        for index in range(len(action)):
            # Tuple of post and prior
            last = self.obs_step(last[0], action[index], embed[index])
            [o.append(l) for l, o in zip(last[0], posts)]
            [o.append(l) for l, o in zip(last[1], priors)]

        prior = [torch.stack(x, dim=0) for x in priors]
        post = [torch.stack(x, dim=0) for x in posts]

        prior = [e.permute(1, 0, 2) for e in prior]
        post = [e.permute(1, 0, 2) for e in post]

        assert len(prior) == 4, "RSSM observe prior length {0}".format(len(prior))
        for i in range(4):
            assert prior[i].ndim == 3 and prior[i].shape[0] == B, "RSSM observe prior {0} shape {1}".format(i,
                                                                                                            prior[
                                                                                                                i].shape)
            assert prior[i].shape[1] == T, "RSSM observe prior {0} shape {1}".format(i, prior[i].shape)
            if i == 3:
                assert prior[i].shape[2] == self.deter_size, "RSSM observe prior {0} shape {1}".format(i, prior[
                    i].shape)
            else:
                assert prior[i].shape[2] == self.stoch_size, "RSSM observe prior {0} shape {1}".format(i, prior[
                    i].shape)

        assert len(post) == 4, "RSSM observe post length {0}".format(len(post))
        for i in range(4):
            assert post[i].ndim == 3 and post[i].shape[0] == B, "RSSM observe post {0} shape {1}".format(i,
                                                                                                         post[
                                                                                                             i].shape)
            assert post[i].shape[1] == T, "RSSM observe post {0} shape {1}".format(i, post[i].shape)
            if i == 3:
                assert post[i].shape[2] == self.deter_size, "RSSM observe post {0} shape {1}".format(i, post[
                    i].shape)
            else:
                assert post[i].shape[2] == self.stoch_size, "RSSM observe post {0} shape {1}".format(i, post[
                    i].shape)

        return post, prior

    def imagine(self, action: TensorType,
                state: List[TensorType] = None) -> List[TensorType]:
        """Imagines the trajectory starting from state through a list of actions.
        Similar to observe(), requires rolling out the RNN for each timestep.

        Args:
            action (TensorType): Actions
            state (List[TensorType]): Starting state before rollout

        Returns:
            Prior states
        """
        if state is None:
            state = self.get_initial_state(action.size()[0])

        assert action.ndim == 3 and action.shape[2] == self.action_size, "RSSM imagine input action shape {0}".format(
            action.shape)
        B, T, _ = action.shape
        assert len(state) == 4, "RSSM imagine input state length {0}".format(len(state))
        for i in range(4):
            assert state[i].ndim == 2 and state[i].shape[0] == B, "RSSM imagine input state {0} shape {1}".format(i,
                                                                                                                  state[
                                                                                                                      i].shape)
            if i == 3:
                assert state[i].shape[1] == self.deter_size, "RSSM imagine input state {0} shape {1}".format(i, state[
                    i].shape)
            else:
                assert state[i].shape[1] == self.stoch_size, "RSSM imagine input state {0} shape {1}".format(i, state[
                    i].shape)

        action = action.permute(1, 0, 2)

        indices = range(len(action))
        priors = [[] for _ in range(len(state))]
        last = state
        for index in indices:
            last = self.img_step(last, action[index])
            [o.append(l) for l, o in zip(last, priors)]

        prior = [torch.stack(x, dim=0) for x in priors]
        prior = [e.permute(1, 0, 2) for e in prior]

        assert len(prior) == 4, "RSSM imagine prior length {0}".format(len(prior))
        for i in range(4):
            assert prior[i].ndim == 3 and prior[i].shape[0] == B, "RSSM imagine prior {0} shape {1}".format(i,
                                                                                                            prior[
                                                                                                                i].shape)
            assert prior[i].shape[1] == T, "RSSM imagine prior {0} shape {1}".format(i, prior[i].shape)
            if i == 3:
                assert prior[i].shape[2] == self.deter_size, "RSSM imagine prior {0} shape {1}".format(i, prior[
                    i].shape)
            else:
                assert prior[i].shape[2] == self.stoch_size, "RSSM imagine prior {0} shape {1}".format(i, prior[
                    i].shape)

        return prior

    def obs_step(
            self, prev_state: TensorType, prev_action: TensorType,
            embed: TensorType) -> Tuple[List[TensorType], List[TensorType]]:
        """Runs through the posterior model and returns the posterior state

        Args:
            prev_state (TensorType): The previous state
            prev_action (TensorType): The previous action
            embed (TensorType): Embedding from ConvEncoder

        Returns:
            Post and Prior state
      """

        assert embed.ndim == 2, "RSSM obs_step input embed shape {0}".format(embed.shape)
        B, _ = embed.shape
        assert prev_action.ndim == 2 and prev_action.shape[0] == B and prev_action.shape[
            1] == self.action_size, "RSSM obs_step input action shape {0}".format(prev_action.shape)
        assert len(prev_state) == 4, "RSSM obs_step input state length {0}".format(len(prev_state))
        for i in range(4):
            assert prev_state[i].ndim == 2 and prev_state[i].shape[
                0] == B, "RSSM obs_step input state {0} shape {1}".format(i, prev_state[i].shape)
            if i == 3:
                assert prev_state[i].shape[1] == self.deter_size, "RSSM obs_step input state {0} shape {1}".format(i,
                                                                                                                   prev_state[
                                                                                                                       i].shape)
            else:
                assert prev_state[i].shape[1] == self.stoch_size, "RSSM obs_step input state {0} shape {1}".format(i,
                                                                                                                   prev_state[
                                                                                                                       i].shape)

        prior = self.img_step(prev_state, prev_action)
        x = torch.cat([prior[3], embed], dim=-1)
        x = self.obs1(x)
        x = self.act()(x)
        x = self.obs2(x)
        mean, std = torch.chunk(x, 2, dim=-1)
        std = self.softplus()(std) + 0.1
        stoch = self.get_dist(mean, std).rsample()
        post = [mean, std, stoch, prior[3]]

        assert len(prior) == 4, "RSSM obs_step prior length {0}".format(len(prior))
        for i in range(4):
            assert prior[i].ndim == 2 and prior[i].shape[0] == B, "RSSM obs_step prior {0} shape {1}".format(i,
                                                                                                             prior[
                                                                                                                 i].shape)
            if i == 3:
                assert prior[i].shape[1] == self.deter_size, "RSSM obs_step prior {0} shape {1}".format(i, prior[
                    i].shape)
            else:
                assert prior[i].shape[1] == self.stoch_size, "RSSM obs_step prior {0} shape {1}".format(i, prior[
                    i].shape)

        assert len(post) == 4, "RSSM obs_step post length {0}".format(len(post))
        for i in range(4):
            assert post[i].ndim == 2 and post[i].shape[0] == B, "RSSM obs_step post {0} shape {1}".format(i,
                                                                                                          post[
                                                                                                              i].shape)
            if i == 3:
                assert post[i].shape[1] == self.deter_size, "RSSM obs_step post {0} shape {1}".format(i, post[
                    i].shape)
            else:
                assert post[i].shape[1] == self.stoch_size, "RSSM obs_step post {0} shape {1}".format(i, post[
                    i].shape)

        return post, prior

    def img_step(self, prev_state: TensorType,
                 prev_action: TensorType) -> List[TensorType]:
        """Runs through the prior model and returns the prior state

        Args:
            prev_state (TensorType): The previous state
            prev_action (TensorType): The previous action

        Returns:
            Prior state
        """
        assert prev_action.ndim == 2 and prev_action.shape[
            1] == self.action_size, "RSSM img_step input action shape {0}".format(prev_action.shape)
        B = prev_action.shape[0]
        assert len(prev_state) == 4, "RSSM img_step input state length {0}".format(len(prev_state))
        for i in range(4):
            assert prev_state[i].ndim == 2 and prev_state[i].shape[
                0] == B, "RSSM img_step input state {0} shape {1}".format(i, prev_state[i].shape)
            if i == 3:
                assert prev_state[i].shape[1] == self.deter_size, "RSSM img_step input state {0} shape {1}".format(i,
                                                                                                                   prev_state[
                                                                                                                       i].shape)
            else:
                assert prev_state[i].shape[1] == self.stoch_size, "RSSM img_step input state {0} shape {1}".format(i,
                                                                                                                   prev_state[
                                                                                                                       i].shape)

        x = torch.cat([prev_state[2], prev_action], dim=-1)
        x = self.img1(x)
        x = self.act()(x)
        deter = self.cell(x, prev_state[3])
        x = deter
        x = self.img2(x)
        x = self.act()(x)
        x = self.img3(x)
        mean, std = torch.chunk(x, 2, dim=-1)
        std = self.softplus()(std) + 0.1
        stoch = self.get_dist(mean, std).rsample()
        prior = [mean, std, stoch, deter]
        assert len(prior) == 4, "RSSM img_step prior length {0}".format(len(prior))
        for i in range(4):
            assert prior[i].ndim == 2 and prior[i].shape[0] == B, "RSSM img_step prior {0} shape {1}".format(i,
                                                                                                             prior[
                                                                                                                 i].shape)
            if i == 3:
                assert prior[i].shape[1] == self.deter_size, "RSSM img_step prior {0} shape {1}".format(i, prior[
                    i].shape)
            else:
                assert prior[i].shape[1] == self.stoch_size, "RSSM img_step prior {0} shape {1}".format(i, prior[
                    i].shape)

        return [mean, std, stoch, deter]

    def get_feature(self, state: List[TensorType]) -> TensorType:
        # Constructs feature for input to reward, decoder, actor, critic
        return torch.cat([state[2], state[3]], dim=-1)

    def get_dist(self, mean: TensorType, std: TensorType) -> TensorType:
        return td.Normal(mean, std)


# Represents all models in Dreamer, unifies them all into a single interface
class DreamerModel(TorchModelV2, nn.Module):
    def __init__(self, obs_space, action_space, num_outputs, model_config,
                 name):
        super().__init__(obs_space, action_space, num_outputs, model_config,
                         name)

        nn.Module.__init__(self)
        self.depth = model_config["depth_size"]
        self.deter_size = model_config["deter_size"]
        self.stoch_size = model_config["stoch_size"]
        self.hidden_size = model_config["hidden_size"]
        if MAZE:
            self.action_size = action_space.n
        else:
            self.action_size = action_space.shape[0]

        self.encoder = ConvEncoder(self.depth)
        self.decoder = ConvDecoder(
            self.stoch_size + self.deter_size, depth=self.depth)
        if MAZE:
            self.mask_decoder = DenseDecoder(self.stoch_size + self.deter_size,
                                             self.action_size, 3, self.hidden_size, dist='binary')
        else:
            self.mask_decoder = None
        self.reward = DenseDecoder(self.stoch_size + self.deter_size, 1, 2,
                                   self.hidden_size)
        self.dynamics = RSSM(
            self.action_size,
            32 * self.depth,
            stoch=self.stoch_size,
            deter=self.deter_size)
        self.actor = ActionDecoder(self.stoch_size + self.deter_size,
                                   self.action_size, 4, self.hidden_size)
        self.value = DenseDecoder(self.stoch_size + self.deter_size, 1, 3,
                                  self.hidden_size)
        self.state = None

        self.device = (torch.device("cuda")
                       if torch.cuda.is_available() else torch.device("cpu"))

    def policy(self, obs: TensorType, mask: TensorType, state: List[TensorType], explore=True
               ) -> Tuple[TensorType, List[float], List[TensorType]]:
        """Returns the action. Runs through the encoder, recurrent model,
        and policy to obtain action.
        """
        if state is None:
            self.initial_state()
        else:
            self.state = state

        assert obs.ndim == 4 and obs.shape[1] == 3 and obs.shape[2] == 64 and obs.shape[
            3] == 64, "DreamerModel policy obs shape {0}".format(obs.shape)
        B = obs.shape[0]
        assert len(state) == 5, "DreamerModel policy state length {0}".format(len(state))
        for i in range(5):
            assert state[i].shape[0] == B, "DreamerModel policy state {0} length {1}".format(i, state[i].shape)
            if i < 3:
                assert state[i].ndim == 2 and state[i].shape[
                    1] == self.stoch_size, "DreamerModel policy state {0} length {1}".format(i, state[i].shape)
            elif i == 3:
                assert state[i].ndim == 2 and state[i].shape[
                    1] == self.deter_size, "DreamerModel policy state {0} length {1}".format(i, state[i].shape)
            else:
                assert state[i].ndim == 2 and state[i].shape[
                        1] == self.action_size, "DreamerModel policy state {0} length {1}".format(i, state[i].shape)

        post = self.state[:4]
        action = self.state[4]

        embed = self.encoder(obs)
        post, _ = self.dynamics.obs_step(post, action, embed)
        feat = self.dynamics.get_feature(post)

        if mask is not None:
            action_dist = self.actor(feat, mask.float())
        else:
            action_dist = self.actor(feat)
        if explore:
            action = action_dist.sample()
        else:
            if MAZE:
                action_dist.logits
            else:
                action = action_dist.mean
        logp = action_dist.log_prob(action)

        self.state = post + [action]

        if MAZE:
            action = action.argmax(-1)

        if MAZE:
            assert action.ndim == 1 and action.shape[0] == B, "DreamerModel action shape {0}".format(action.shape)
        else:
            assert action.ndim == 2 and action.shape[0] == B and action.shape[
                1] == self.action_size, "DreamerModel action shape {0}".format(action.shape)
        assert logp.ndim == 1 and logp.shape[0] == B, "DreamerModel logp shape {0}".format(logp.shape)
        for i in range(5):
            assert self.state[i].shape[0] == B, "DreamerModel policy output state {0} length {1}".format(i, self.state[
                i].shape)
            if i < 3:
                assert self.state[i].ndim == 2 and self.state[i].shape[
                    1] == self.stoch_size, "DreamerModel policy output state {0} length {1}".format(i,
                                                                                                    self.state[i].shape)
            elif i == 3:
                assert self.state[i].ndim == 2 and self.state[i].shape[
                    1] == self.deter_size, "DreamerModel policy output state {0} length {1}".format(i,
                                                                                                    self.state[i].shape)
            else:
                assert self.state[i].ndim == 2 and self.state[i].shape[
                        1] == self.action_size, "DreamerModel policy output state {0} length {1}".format(i, self.state[
                        i].shape)

        return action, logp, self.state

    def imagine_ahead(self, state: List[TensorType],
                      horizon: int) -> TensorType:
        """Given a batch of states, rolls out more state of length horizon.
        """
        a = state[0].shape[0]
        b = state[0].shape[1]
        assert len(state) == 4, "imagine_ahead state length {0}".format(len(state))
        for i in range(4):
            assert state[i].ndim == 3, "imagine_ahead state {0} shape {1}".format(i, state[i].shape)
            assert state[i].shape[0] == a, "imagine_ahead state {0} shape {1}".format(i, state[i].shape)
            assert state[i].shape[1] == b, "imagine_ahead state {0} shape {1}".format(i, state[i].shape)
            if i == 3:
                assert state[i].shape[2] == self.deter_size, "imagine_ahead state {0} shape {1}".format(i,
                                                                                                        state[i].shape)
            else:
                assert state[i].shape[2] == self.stoch_size, "imagine_ahead state {0} shape {1}".format(i,
                                                                                                        state[i].shape)
        start = []
        for s in state:
            s = s.contiguous().detach()
            shpe = [-1] + list(s.size())[2:]
            start.append(s.view(*shpe))

        def next_state(current_state):
            feature = self.dynamics.get_feature(current_state).detach()
            if MAZE:
                pred_mask = self.mask_decoder(feature).sample()
                action = self.actor(feature, pred_mask).rsample()
            else:
                action = self.actor(feature).rsample()
            new_state = self.dynamics.img_step(current_state, action)
            return new_state

        last = start
        outputs = [[] for _ in range(len(start))]
        for _ in range(horizon):
            last = next_state(last)
            [o.append(l) for l, o in zip(last, outputs)]
        outputs = [torch.stack(x, dim=0) for x in outputs]

        imag_feat = self.dynamics.get_feature(outputs)

        assert imag_feat.ndim == 3 and imag_feat.shape[0] == horizon and imag_feat.shape[1] == a * b and \
               imag_feat.shape[2] == self.stoch_size + self.deter_size, "DreamerModel imag_feat shape {0}".format(
            imag_feat.shape)

        return imag_feat

    def get_initial_state(self) -> List[TensorType]:
        self.state = self.dynamics.get_initial_state(1) + [
                torch.zeros(1, self.action_size).to(self.device)
            ]
        return self.state

    def value_function(self) -> TensorType:
        return None
